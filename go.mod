module example

go 1.14

require (
	bitbucket.org/funplus/ark v0.0.3-0.20201126103729-1031aec762f6
	bitbucket.org/funplus/golib v0.2.8-0.20201216061523-2d224353e2c3
	bitbucket.org/funplus/sandwich v0.1.1-0.20201216093551-85e73fddb400
	github.com/golang/protobuf v1.4.3
	github.com/smartystreets/goconvey v1.6.4
	github.com/vmihailenco/msgpack/v5 v5.1.3 // indirect
	go.etcd.io/etcd/v3 v3.3.0-rc.0.0.20200720071305-89da79188f73
	google.golang.org/genproto v0.0.0-20200904004341-0bd0a958aa1d
	google.golang.org/protobuf v1.25.1-0.20200805231151-a709e31e5d12
)

replace google.golang.org/grpc => google.golang.org/grpc v1.27.0 // for etcd

