// ======================================
// ENGINE LEVEL PACKET. DO NOT EDIT.
// ======================================

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.23.0
// 	protoc        v3.12.4
// source: netutils/cmds.proto

package netutils

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

// for internal use
type CmdPing struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Timestamp int64 `protobuf:"varint,1,opt,name=timestamp,proto3" json:"timestamp,omitempty"`
}

func (x *CmdPing) Reset() {
	*x = CmdPing{}
	if protoimpl.UnsafeEnabled {
		mi := &file_netutils_cmds_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CmdPing) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CmdPing) ProtoMessage() {}

func (x *CmdPing) ProtoReflect() protoreflect.Message {
	mi := &file_netutils_cmds_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CmdPing.ProtoReflect.Descriptor instead.
func (*CmdPing) Descriptor() ([]byte, []int) {
	return file_netutils_cmds_proto_rawDescGZIP(), []int{0}
}

func (x *CmdPing) GetTimestamp() int64 {
	if x != nil {
		return x.Timestamp
	}
	return 0
}

type CmdPingAck struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Timestamp int64 `protobuf:"varint,1,opt,name=timestamp,proto3" json:"timestamp,omitempty"`
}

func (x *CmdPingAck) Reset() {
	*x = CmdPingAck{}
	if protoimpl.UnsafeEnabled {
		mi := &file_netutils_cmds_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CmdPingAck) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CmdPingAck) ProtoMessage() {}

func (x *CmdPingAck) ProtoReflect() protoreflect.Message {
	mi := &file_netutils_cmds_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CmdPingAck.ProtoReflect.Descriptor instead.
func (*CmdPingAck) Descriptor() ([]byte, []int) {
	return file_netutils_cmds_proto_rawDescGZIP(), []int{1}
}

func (x *CmdPingAck) GetTimestamp() int64 {
	if x != nil {
		return x.Timestamp
	}
	return 0
}

// for devops checkup
type CmdCheckup struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code               int32  `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message            string `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	CustomMeasurements []byte `protobuf:"bytes,3,opt,name=CustomMeasurements,proto3" json:"CustomMeasurements,omitempty"`
}

func (x *CmdCheckup) Reset() {
	*x = CmdCheckup{}
	if protoimpl.UnsafeEnabled {
		mi := &file_netutils_cmds_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CmdCheckup) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CmdCheckup) ProtoMessage() {}

func (x *CmdCheckup) ProtoReflect() protoreflect.Message {
	mi := &file_netutils_cmds_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CmdCheckup.ProtoReflect.Descriptor instead.
func (*CmdCheckup) Descriptor() ([]byte, []int) {
	return file_netutils_cmds_proto_rawDescGZIP(), []int{2}
}

func (x *CmdCheckup) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *CmdCheckup) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *CmdCheckup) GetCustomMeasurements() []byte {
	if x != nil {
		return x.CustomMeasurements
	}
	return nil
}

var File_netutils_cmds_proto protoreflect.FileDescriptor

var file_netutils_cmds_proto_rawDesc = []byte{
	0x0a, 0x13, 0x6e, 0x65, 0x74, 0x75, 0x74, 0x69, 0x6c, 0x73, 0x2f, 0x63, 0x6d, 0x64, 0x73, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x08, 0x6e, 0x65, 0x74, 0x75, 0x74, 0x69, 0x6c, 0x73, 0x22,
	0x27, 0x0a, 0x07, 0x43, 0x6d, 0x64, 0x50, 0x69, 0x6e, 0x67, 0x12, 0x1c, 0x0a, 0x09, 0x74, 0x69,
	0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x74,
	0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x22, 0x2a, 0x0a, 0x0a, 0x43, 0x6d, 0x64, 0x50,
	0x69, 0x6e, 0x67, 0x41, 0x63, 0x6b, 0x12, 0x1c, 0x0a, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74,
	0x61, 0x6d, 0x70, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x73,
	0x74, 0x61, 0x6d, 0x70, 0x22, 0x6a, 0x0a, 0x0a, 0x43, 0x6d, 0x64, 0x43, 0x68, 0x65, 0x63, 0x6b,
	0x75, 0x70, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05,
	0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
	0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x12, 0x2e, 0x0a, 0x12, 0x43, 0x75, 0x73, 0x74, 0x6f, 0x6d, 0x4d, 0x65, 0x61, 0x73, 0x75, 0x72,
	0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x12, 0x43, 0x75,
	0x73, 0x74, 0x6f, 0x6d, 0x4d, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x73,
	0x42, 0x41, 0x5a, 0x30, 0x62, 0x69, 0x74, 0x62, 0x75, 0x63, 0x6b, 0x65, 0x74, 0x2e, 0x6f, 0x72,
	0x67, 0x2f, 0x66, 0x75, 0x6e, 0x70, 0x6c, 0x75, 0x73, 0x2f, 0x73, 0x61, 0x6e, 0x64, 0x77, 0x69,
	0x63, 0x68, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x6f, 0x6c, 0x2f, 0x6e, 0x65, 0x74, 0x75,
	0x74, 0x69, 0x6c, 0x73, 0xaa, 0x02, 0x0c, 0x67, 0x65, 0x6e, 0x2e, 0x6e, 0x65, 0x74, 0x75, 0x74,
	0x69, 0x6c, 0x73, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_netutils_cmds_proto_rawDescOnce sync.Once
	file_netutils_cmds_proto_rawDescData = file_netutils_cmds_proto_rawDesc
)

func file_netutils_cmds_proto_rawDescGZIP() []byte {
	file_netutils_cmds_proto_rawDescOnce.Do(func() {
		file_netutils_cmds_proto_rawDescData = protoimpl.X.CompressGZIP(file_netutils_cmds_proto_rawDescData)
	})
	return file_netutils_cmds_proto_rawDescData
}

var file_netutils_cmds_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_netutils_cmds_proto_goTypes = []interface{}{
	(*CmdPing)(nil),    // 0: netutils.CmdPing
	(*CmdPingAck)(nil), // 1: netutils.CmdPingAck
	(*CmdCheckup)(nil), // 2: netutils.CmdCheckup
}
var file_netutils_cmds_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_netutils_cmds_proto_init() }
func file_netutils_cmds_proto_init() {
	if File_netutils_cmds_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_netutils_cmds_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CmdPing); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_netutils_cmds_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CmdPingAck); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_netutils_cmds_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CmdCheckup); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_netutils_cmds_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_netutils_cmds_proto_goTypes,
		DependencyIndexes: file_netutils_cmds_proto_depIdxs,
		MessageInfos:      file_netutils_cmds_proto_msgTypes,
	}.Build()
	File_netutils_cmds_proto = out.File
	file_netutils_cmds_proto_rawDesc = nil
	file_netutils_cmds_proto_goTypes = nil
	file_netutils_cmds_proto_depIdxs = nil
}
