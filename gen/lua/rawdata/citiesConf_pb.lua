-- Code generated by ProtoKitGo. DO NOT EDIT.
-- input: rawdata/citiesConf.proto

local pb = require "pb"
local protoc = require "protoc"
local protoStr = [==[
// Code generated by ProtoKit. DO NOT EDIT. (client)

// from sheet@FCT v0.0.1 config|cities@design
syntax = "proto3";
package rawdata;
option go_package = "example/gen/golang/rawdata";
option csharp_namespace = "gen.rawdata";


message cities{
  // // 场景id
  int32 City_id = 1;
  // 每小时基础cash产出
  int32 base_hourly_money = 2;
  // 单人每小时coal产出
  float base_personal_hourly_coal = 3;
  // 基础温度
  int32 base_temperature = 4;
  // 初始煤炭存量
  int32 initial_coal = 5;
  // 启动资金
  int32 initial_money = 6;
  // 初始生食材
  int32 initial_raw_food = 7;
  // 场景prefab
  string scene = 8;
}

message citiesConf{
  repeated cities citiesSlice = 1;
}

]==]
assert(protoc:load(protoStr,"rawdata/citiesConf.proto"))

---@class cities
---@field public City_id number
---@field public base_hourly_money number
---@field public base_personal_hourly_coal number
---@field public base_temperature number
---@field public initial_coal number
---@field public initial_money number
---@field public initial_raw_food number
---@field public scene string
local cities={}
cities.__index = cities
---@return cities
function cities:new(data) return setmetatable(data or {},cities)  end
---@return cities
function cities:newFromBytes(bytes) return setmetatable(pb.decode(self:getMessageName(),bytes) or {},cities)  end
---@return string
function cities:getMessageName() return "rawdata.cities" end
---@return string
function cities:marshal()  return pb.encode(self:getMessageName(),self) end
rawdata.cities = cities

---@class citiesConf
---@field public citiesSlice cities[]
local citiesConf={}
citiesConf.__index = citiesConf
---@return citiesConf
function citiesConf:new(data) return setmetatable(data or {},citiesConf)  end
---@return citiesConf
function citiesConf:newFromBytes(bytes) return setmetatable(pb.decode(self:getMessageName(),bytes) or {},citiesConf)  end
---@return string
function citiesConf:getMessageName() return "rawdata.citiesConf" end
---@return string
function citiesConf:marshal()  return pb.encode(self:getMessageName(),self) end
rawdata.citiesConf = citiesConf

